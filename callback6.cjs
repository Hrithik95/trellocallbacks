const boardIdInfoFunc = require('./callback1.cjs');
const allListFunc = require('./callback2.cjs');
const allCardsFunc = require('./callback3.cjs');

function problem6() {
    setTimeout(() => {
        boardIdInfoFunc("mcu453ed", (error, thanosInfo) => {
            if (error) {
                console.error(error);
            } else {
                console.log(thanosInfo);
                
                allListFunc("mcu453ed", (error, thanosBoardLists) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(thanosBoardLists);
                        const thanosBoardListsArray = Object.entries(thanosBoardLists);

                        thanosBoardListsArray.map((element) => {
                            let listID = element[1]["id"];
                            allCardsFunc(listID, (error, cardData) => {
                                if (error) {
                                    console.error(error);
                                } else {
                                    console.log(cardData);
                                }
                            });
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}

module.exports = problem6;