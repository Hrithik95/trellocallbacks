//Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.


const fs = require('fs');

function allLists(boardId, callback) {
    setTimeout(() => {
        fs.readFile('../lists.json', 'utf-8', (error, data) => {
            if (error) {
                callback(error);
            } else {
                const listData = JSON.parse(data);
                const listDataArray = Object.entries(listData);
                listDataArray.map((element, index) => {
                    if (element[0] === boardId) {
                        callback(null, element[1]);
                    }
                });
            }
        });
    }, 2 * 1000);
}

module.exports = allLists;