//Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const fs = require('fs');

function allCards(listID, callback) {
    setTimeout(() => {
        fs.readFile('../cards.json', 'utf-8', (error, data) => {
            if (error) {
                callback(error);
            } else {
                const cardData = JSON.parse(data);
                const cardDataArray = Object.entries(cardData);
                cardDataArray.map((element, index) => {
                    if (element[0] === listID) {
                        callback(null, element[1]);
                    }
                });
            }
        });
    }, 2 * 1000);
}

module.exports = allCards;