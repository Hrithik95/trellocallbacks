// Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind list simultaneously

const boardIdInfoFunc = require('./callback1.cjs');
const allListFunc = require('./callback2.cjs');
const allCardsFunc = require('./callback3.cjs');

function problem4() {
    setTimeout(() => {
        boardIdInfoFunc("mcu453ed", (error, thanosInfo) => {
            if (error) {
                console.error(error);
            } else {
                console.log(thanosInfo);
                allListFunc("mcu453ed", (error, thanosBoardLists) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(thanosBoardLists);
                        allCardsFunc("qwsa221", (error, mindCards) => {
                            if (error) {
                                console.error(error);
                            } else {
                                console.log(mindCards);
                            }
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}

module.exports = problem4;