const testCallbackTwoFunction = require('../callback2.cjs');

testCallbackTwoFunction("abc122dc", (error, data) => {
    if (error) {
        console.error(error);
    } else {
        console.log(data);
    }
});