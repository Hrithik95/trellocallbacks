const testCallback1 = require('../callback1.cjs');

testCallback1("abc122dc", (error, data) => {
    if (error) {
        console.error(error);
    } else {
        console.log(data);
    }
});