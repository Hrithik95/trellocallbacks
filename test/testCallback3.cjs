const { error } = require('console');
const testCallbackThreeFunc = require('../callback3.cjs');

testCallbackThreeFunc("qwsa221", (error,data)=>{
    if(error){
        console.error(error);
    }else{
        console.log(data);
    }
});