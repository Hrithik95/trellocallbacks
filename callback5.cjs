const boardIdInfoFunc = require('./callback1.cjs');
const allListFunc = require('./callback2.cjs');
const allCardsFunc = require('./callback3.cjs');

function problem5() {
    setTimeout(() => {
        boardIdInfoFunc("mcu453ed", (error, thanosInfo) => {
            if (error) {
                console.error(error);
            } else {
                console.log(thanosInfo);
                allListFunc("mcu453ed", (error, thanosBoardLists) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(thanosBoardLists);
                        allCardsFunc("qwsa221", (error, mindCards) => {
                            if (error) {
                                console.error(error);
                            } else {
                                console.log(mindCards);
                                allCardsFunc("jwkh245", (error, spaceCards) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(spaceCards);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}

module.exports = problem5;