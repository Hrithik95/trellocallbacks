// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const fs = require('fs');

function boardInformation(boardID, callback) {
    setTimeout(() => {
        fs.readFile('../boards.json', 'utf-8', (error, data) => {
            if (error) {
                callback(error);
            } else {
                const boardData = JSON.parse(data);
                boardData.map((item) => {
                    if (item.id === boardID) {
                        callback(null, item);
                        return;
                    }
                });

            }
        });
    }, 2 * 1000);
}

module.exports = boardInformation;